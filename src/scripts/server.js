import { createServer, Model } from "miragejs";

export default function () {
  createServer({
    models: {
      subscriber: Model,
    },

    routes() {
      this.get("api/subscribers", (schema) => {
        return schema.subscribers.all();
      });

      this.post("api/subscribers", (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        return schema.subscribers.create(attrs);
      });

      this.patch("api/subscribers/:id", (schema, request) => {
        let newAttrs = JSON.parse(request.requestBody);
        let subscriberId = request.params.id;
        let subscriber = schema.subscribers.find(subscriberId);
        return subscriber.update(newAttrs);
      });

      this.delete("api/subscribers/:id", (schema, request) => {
        let subscriberId = request.params.id;
        return schema.subscribers.find(subscriberId).destroy();
      });
    },
  });
}
