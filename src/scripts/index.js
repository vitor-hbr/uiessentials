import "../css/index.scss";
import makeServer from "./server";
import { addSubscriber } from "./requests";

makeServer();

let form;

const sendSubscribersInSession = async () => {
  const subscriberArray = JSON.parse(sessionStorage.getItem("subscriberArray"));
  if (subscriberArray && Array.isArray(subscriberArray)) {
    subscriberArray.forEach((subscriber, index) => {
      subscriber.id = String(index + 1);
      addSubscriber({ name: subscriber.name, email: subscriber.email });
    });
    sessionStorage.setItem("subscriberArray", JSON.stringify(subscriberArray));
  }
};

const addSubscriberToStorage = (subscriber) => {
  let subscriberArray = [];
  const subscriberArrayFromSession = JSON.parse(
    sessionStorage.getItem("subscriberArray")
  );
  if (subscriberArrayFromSession) {
    subscriberArrayFromSession.push(subscriber);
    sessionStorage.setItem(
      "subscriberArray",
      JSON.stringify(subscriberArrayFromSession)
    );
  } else {
    subscriberArray.push(subscriber);
    sessionStorage.setItem("subscriberArray", JSON.stringify(subscriberArray));
  }
};

const addEventListenerForSubmit = () => {
  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    const name = document.getElementById("nameInput").value;
    const email = document.getElementById("emailInput").value;
    await addSubscriber({ name: name, email: email }).then((response) => {
      addSubscriberToStorage(response.data.subscriber);
    });
    alert(`Olá ${name}, parabéns você se inscreveu na nossa newsletter!`);
  });
};

const preventSubmit = () => {
  document
    .getElementById("form-button")
    .addEventListener("click", function (event) {});
};

document.addEventListener("DOMContentLoaded", function (event) {
  form = document.getElementById("form");
  preventSubmit();
  sendSubscribersInSession();
  addEventListenerForSubmit();
});
