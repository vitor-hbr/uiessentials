import "../css/index.scss";
import "../css/admin.scss";
import makeServer from "./server";

import {
  getAllSubscribers,
  updateSubscriber,
  deleteSubscriber,
  addSubscriber,
} from "./requests";

makeServer();

let subscribersBackupData = [];

const showEmptyNewsletterMessage = () => {
  const message = document.querySelector(".empty-newsletter");
  const header = document.querySelector(".container");
  message.classList.remove("hidden");
  header.classList.add("hidden");
};

const sendSubscribersInSession = async () => {
  const subscriberArray = JSON.parse(sessionStorage.getItem("subscriberArray"));
  if (subscriberArray && Array.isArray(subscriberArray)) {
    if (subscriberArray.length === 0) {
      showEmptyNewsletterMessage();
    }
    subscriberArray.forEach((subscriber, index) => {
      subscriber.id = String(index + 1);
      addSubscriber({ name: subscriber.name, email: subscriber.email });
    });
    sessionStorage.setItem("subscriberArray", JSON.stringify(subscriberArray));
  }
};

const updateSubscriberFromSession = (subscriber, id) => {
  const subscriberArray = JSON.parse(sessionStorage.getItem("subscriberArray"));
  if (subscriberArray && Array.isArray(subscriberArray)) {
    searchSubscriberArrayAndUpdate(subscriberArray, id);
    sessionStorage.setItem("subscriberArray", JSON.stringify(subscriberArray));
  }
};

const deleteSubscriberFromSession = (id) => {
  const subscriberArray = JSON.parse(sessionStorage.getItem("subscriberArray"));
  if (subscriberArray && Array.isArray(subscriberArray)) {
    const newSubscriberArray = searchSubscriberArrayAndDelete(
      subscriberArray,
      id
    );
    if (newSubscriberArray.length === 0) {
      showEmptyNewsletterMessage();
    }
    sessionStorage.setItem(
      "subscriberArray",
      JSON.stringify(newSubscriberArray)
    );
  }
};

const searchSubscriberArrayAndDelete = (array, id) => {
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        array.splice(i, 1);
        break;
      }
    }
    return array;
  }
};

const searchSubscriberArrayAndGet = (array, id) => {
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        return array[i];
      }
    }
  }
};

const searchSubscriberArrayAndUpdate = (array, id, updatedSubscriber) => {
  if (Array.isArray(array)) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        array[i].name = updatedSubscriber.name;
        array[i].email = updatedSubscriber.email;
        break;
      }
    }
  }
};

const loadSubscribers = async () => {
  const subscriberList = document.querySelector(".subscriber-list");
  const subscriberItem = document.querySelector(".subscriber-item");
  getAllSubscribers().then((response) => {
    subscribersBackupData = response.subscribers;
    response.subscribers.forEach((subscriber) => {
      const newSubscriber = subscriberItem.cloneNode(true);
      newSubscriber.id = subscriber.id;
      const div = newSubscriber.children[0];
      div.children[0].value = subscriber.name;
      div.children[1].value = subscriber.email;
      addEventListenerItemButtons(newSubscriber);
      subscriberList.appendChild(newSubscriber);
    });
  });
  subscriberList.removeChild(subscriberItem);
};

const toggleEdit = (childrenNodes) => {
  for (let index = 0; index < childrenNodes.length; index++) {
    if (childrenNodes[index].tagName === "INPUT") {
      childrenNodes[index].toggleAttribute("disabled");
      childrenNodes[index].classList.toggle("has-border");
    } else if (childrenNodes[index].tagName === "BUTTON") {
      childrenNodes[index].classList.toggle("hidden");
    }
  }
};

const addEventListenerItemButtons = (subscriberItem) => {
  const id = subscriberItem.id;
  const div = subscriberItem.children[0];

  const editButton = div.children[2];
  editButton.addEventListener("click", () => {
    toggleEdit(div.children);
  });

  const deleteButton = div.children[3];
  deleteButton.addEventListener("click", () => {
    deleteSubscriberFromSession(id);
    searchSubscriberArrayAndDelete(subscribersBackupData, id);
    deleteSubscriber(id);
    subscriberItem.parentNode.removeChild(subscriberItem);
  });

  const confirmEditButton = div.children[4];
  confirmEditButton.addEventListener("click", () => {
    const subscriber = {
      name: div.children[0].value,
      email: div.children[1].value,
    };
    updateSubscriber(id, subscriber);
    updateSubscriberFromSession(id, subscriber);
    searchSubscriberArrayAndUpdate(subscribersBackupData, id, subscriber);
    toggleEdit(div.children);
  });

  const resetButton = div.children[5];
  resetButton.addEventListener("click", () => {
    toggleEdit(div.children);
    const subscriber = searchSubscriberArrayAndGet(subscribersBackupData, id);
    div.children[0].value = subscriber.name;
    div.children[1].value = subscriber.email;
  });
};

document.addEventListener("DOMContentLoaded", async function () {
  await sendSubscribersInSession();
  await loadSubscribers();
});
