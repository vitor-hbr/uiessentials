import axios from "axios";

export const getAllSubscribers = async () => {
  try {
    const response = await axios.get("/api/subscribers");
    return response.data;
  } catch (e) {
    console.error(e);
  }
};

export const addSubscriber = async (subscriber) => {
  try {
    const response = await axios.post(`/api/subscribers`, subscriber);
    return response;
  } catch (e) {
    console.error(e);
  }
};

export const updateSubscriber = async (id, subscriber) => {
  try {
    const response = await axios.patch(`/api/subscribers/${id}`, subscriber);
  } catch (e) {
    console.error(e);
  }
};

export const deleteSubscriber = async (id) => {
  try {
    const response = await axios.delete(`/api/subscribers/${id}`);
  } catch (e) {
    console.error(e);
  }
};
